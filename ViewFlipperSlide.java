package com.chen.testandroid;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import android.R.integer;
import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class ViewFlipperSlide extends Activity{

	private ViewFlipper flipper;
	private AssetManager asset;
	private List<String> images;
	private LayoutInflater inflater;
	private ImageView image;
	private int index=0;
	private InputStream in;
	private float mStartX;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.flip);
		
		flipper=(ViewFlipper) findViewById(R.id.news_body_flipper);
		
		asset=getAssets();
		try {
			images=Arrays.asList(asset.list("images"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		inflater=getLayoutInflater();
		View view=inflater.inflate(R.layout.image, null);
		image=(ImageView) view.findViewById(R.id.image);
		
		try {
			in=asset.open("images/"+images.get(index));
			Bitmap bitmap=BitmapFactory.decodeStream(in);
			image.setImageBitmap(bitmap);
		} catch (IOException e) {
			e.printStackTrace();
		}
		flipper.addView(view, index);
		image.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch(event.getAction()){
				case MotionEvent.ACTION_DOWN:
					// 记录起始坐标
					mStartX = event.getX();
					break;
				case MotionEvent.ACTION_UP:
					if(event.getX()>mStartX){  //右滑
						showPrevious();
					}else if(event.getX()<mStartX){
						showNext();
					}
					break;
				}
				return true;
			}
		});
	}
	
	
	protected void showPrevious() {
		if(index>0){
			index--;
			try {
				in=asset.open("images/"+images.get(index));
			} catch (IOException e) {
				e.printStackTrace();
			}
			Bitmap bitmap=BitmapFactory.decodeStream(in);
			image.setImageBitmap(bitmap);
			flipper.setInAnimation(this, R.anim.push_right_in);// 定义下一页进来时的动画
			flipper.setOutAnimation(this, R.anim.push_right_out);// 定义当前页出去的动画
			flipper.showNext();
		}else {
			Toast.makeText(ViewFlipperSlide.this, "没有图片了", Toast.LENGTH_SHORT).show();
		}
	}
	//右滑
	protected void showNext() {
		if(index<images.size()-1){
			index++;
			try {
				in=asset.open("images/"+images.get(index));
			} catch (IOException e) {
				e.printStackTrace();
			}
			Bitmap bitmap=BitmapFactory.decodeStream(in);
			image.setImageBitmap(bitmap);
			flipper.setInAnimation(this, R.anim.push_left_in);// 定义下一页进来时的动画
			flipper.setOutAnimation(this, R.anim.push_left_out);// 定义当前页出去的动画
			flipper.showNext();
		}else {
			Toast.makeText(ViewFlipperSlide.this, "没有图片了", Toast.LENGTH_SHORT).show();
		}
	}
}
